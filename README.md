# tails-comparison

Python script for comparing Tails systems, in the form of .img files or block devices (e.g. /dev/sdb).

Related issue: [#7496](https://gitlab.tails.boum.org/tails/tails/-/issues/7496)

# Usage

* Compare a Tails key with a downloaded Tails image:

`python3 tails_comparison.py /dev/sdb tails_image.img`

* Compare two Tails keys:

`python3 tails_comparison.py /dev/sdb /dev/sdc`

*Note: it isn't possible to compare the currently running Tails system with another Tails system (if you see a way to do that, ploup is interested).*

# How does it work

**What the script does:**

* For each of the two systems provided as arguments:
  * Temporary folders with randomly generated names are created in /tmp to serve as mount points
  * The first partition of the image file / block device is mounted on a temporary folder (the script calls this the "boot directory")
  * The `live/Tails.module` file is parsed, and the required squashfs files are mounted on a temporary folder (the script calls this the "root directory")
    * If the only listed module is `filesystem.squashfs` (such as in an .img file), it is mounted alone
    * If several modules are listed (such as in a Tails that has been upgraded), the squashfs files are mounted together using overlayfs
* The contents of the "boot directories" and the "root directories" of both systems are compared
  * Changes in permissions and owners are detected
  * The contents of the individual files are compared using SHA256 hashes
  * Files that are only present in one system or the other are detected and reported
* If the Tails systems aren't identical, mismatched files are reported in the log file `tails_comparison.log`
* At the end of the script, or when encountering an exception, everything is cleaned up
  * Mount points are unmounted
  * Temporary folders are deleted

**What the script does not, but should be doing:**

See the list of [issues](https://gitlab.tails.boum.org/ploup/tails-comparison/-/issues).

**What the script does not, but it is probably not useful (?):**

* Allow comparing old versions of Tails (that are not using the incremental update system)

# Requirements

* python >= 3.4
* python `sh` module (which is included in Tails)
* shell commands `mount`, `umount` and `losetup`, which should be available everywhere

# Example output

In this example, the script was used to compare a Tails USB device that has been upgraded multiple times, up to the 4.6 version, to a freshly downloaded Tails 4.6 image file. We can see that the systems are almost identical, apart from a few files that differ for understandable reasons.

```
root@amnesia:/home/amnesia/Persistent/tails-comparison# python3 tails_comparison.py /dev/sdc ../tails-amd64-4.6.img 
Identified the first system as a block device with Tails 4.6 - 20200504
Identified the second system as an image file with Tails 4.6 - 20200504
Comparing the boot directories... DIFFERENT
Out of 566 files, 4 do NOT match.
See tails_comparison.log for the list of mismatching files.
Comparing the root directories... IDENTICAL

root@amnesia:/home/amnesia/Persistent/tails-comparison# cat tails_comparison.log 
/boot/.disk/info: different
/boot/live/filesystem.packages: different
/boot/syslinux/ldlinux.sys: different
/boot/tmp: only present in the first system
```
