#!/usr/bin/python3

import enum
import pathlib
import sh
import tempfile

class TailsSystem:
  """Represents a tails system, either from an .img file or a block device (e.g. /dev/sdb)."""
  
  class Type(enum.Enum):
    image = 1
    blockDevice = 2

  def __init__(self, path):
    # Set the path
    self.path = pathlib.Path(path)
    
    # If the path doesn't exist
    if not self.path.exists():
      # Raise a runtime error
      raise RuntimeError("File " + str(self.path) + " does not exist.")

  def __enter__(self):
    # If the path is a file
    if self.path.is_file():
      # Consider the system is an image file
      self.type = self.Type.image
    # Else, if the path is a block device
    elif self.path.is_block_device():
      # Consider the system is a block device
      self.type = self.Type.blockDevice

    # Create the boot and root directories
    self.bootDirectory = tempfile.TemporaryDirectory()
    self.rootDirectory = tempfile.TemporaryDirectory()
    self.boot = pathlib.Path(self.bootDirectory.name)
    self.root = pathlib.Path(self.rootDirectory.name)

    # If the system is an image file
    if self.type == self.Type.image:
      # Consider the system is an image file
      self.type = self.Type.image
    
      # Attach the image to a loop device using losetup
      self.loopDevice = pathlib.Path(sh.losetup("-Pf", "--show", self.path).stdout.decode("utf-8").rstrip())
      
      # Mount the boot directory
      sh.mount("-o", "ro,loop", str(self.loopDevice) + "p1", str(self.boot))
    # Else, if the path is a block device
    elif self.type == self.Type.blockDevice:
      # Consider the system is a block device
      self.type = self.Type.blockDevice
    
      # Mount the boot directory
      sh.mount("-o", "ro", str(self.path) + "1", str(self.boot))

    # Create a list of the module names, taken in reverse order from live/Tails.module
    self.moduleNames = list(reversed(open(self.boot.joinpath("live/Tails.module"), "r").read().rstrip().split("\n")))
    
    # Create the modules
    self.modules = list()

    # Iterate in reverse order over the modules listed in live/Tails.module
    for module in self.moduleNames:
      # Create and mount the module directory
      self.modules.append(tempfile.TemporaryDirectory())
      sh.mount("-o", "ro,loop", self.boot.joinpath("live").joinpath(module), self.modules[-1].name)
      
    # If there is only one module
    if(len(self.modules) == 1):
      # Mount the module directory on the root directory
      sh.mount("--bind", self.modules[0].name, str(self.root))
    # Else, there are multiple modules
    else:
      # Mount all the modules directories on the root directory together using overlayfs
      sh.mount("-t", "overlay", "overlay", "-o", "lowerdir=" + ":".join([module.name for module in self.modules]), str(self.root))
      
    # Compute the version
    self.version = open(self.root.joinpath("etc/amnesia/version")).readline().rstrip()
      
    return self

  def __exit__(self, exc_type, exc_val, exc_tb):
    # Unmount the root directory
    sh.umount("-q", str(self.root))
    
    # Unmount the modules directories
    for module in self.modules:
      sh.umount("-q", module.name)
      
    # Unmount the boot directory
    sh.umount("-q", str(self.boot))
    
    # If the system is an image file
    if self.type == self.Type.image:
      # Detach the loop device
      sh.losetup("-d", self.loopDevice)
