#!/usr/bin/python3

import argparse
import filecmp
import pathlib
import sh
import sys

from directories_comparison import DirectoriesComparison
from tails_system import TailsSystem

class TailsComparison:
  """Compare two Tails systems."""

  def __init__(self, firstSystemPath, secondSystemPath):
    # Set the log file path
    self.logFilePath = pathlib.Path("tails_comparison.log")
  
    # Open the log file
    self.logFile = open(self.logFilePath, "w")
  
    # Open the first system
    with TailsSystem(firstSystemPath) as firstSystem:
      # Print the first system identification
      self.printSystemIdentification(firstSystem, "first")
      
      # Open the second system
      with TailsSystem(secondSystemPath) as secondSystem:
        # Print the second system identification
        self.printSystemIdentification(secondSystem, "second")
        
        # Create the list of files to exclude from the comparison of boot directories
        bootDirectoriesComparisonExcludedFiles = [pathlib.Path("live/Tails.module")]
        for module in firstSystem.moduleNames + secondSystem.moduleNames:
          bootDirectoriesComparisonExcludedFiles.append(pathlib.Path("live").joinpath(module))
        
        # Compare the boot directories
        self.compareDirectories(firstSystem.boot, secondSystem.boot, bootDirectoriesComparisonExcludedFiles, pathlib.Path("/boot"), "Comparing the boot directories... ")
        
        # Compare the root directories
        self.compareDirectories(firstSystem.root, secondSystem.root, list(), "/", "Comparing the root directories... ")
        
  def printSystemIdentification(self, system, whichOne):
    """Print the identification of a Tails system."""
    # Print the beginning of the identification
    print("Identified the " + whichOne + " system as ", end="")
    
    # If the system is an image
    if system.type == TailsSystem.Type.image:
      print("an image file", end="")
    # Else, if the system is a block device
    elif system.type == TailsSystem.Type.blockDevice:
      print("a block device", end="")
      
    # Print the end of the identification
    print(" with Tails " + system.version)
    
  def compareDirectories(self, firstDirectory, secondDirectory, excludedFiles, directoryRoot, text):
    """Compare the contents of two directories, excluding a list of files."""
    # Compare the first and second directories
    result = DirectoriesComparison(firstDirectory, secondDirectory, excludedFiles, text)
    
    # Erase the line
    sys.stdout.write("\r\033[K")

    # If all files are identical
    if len(result.identicalFiles) == len(result.files):
      # Print a message
      sys.stdout.write("\r" + text + "IDENTICAL\n")
    # Else
    else:
      # Print a message
      sys.stdout.write("\r" + text + "DIFFERENT\n")
      
      # Compute the number of mismatching files
      mismatchingFilesNumber = len(result.differentFiles) + len(result.filesMissingInSecondDirectory) + len(result.filesMissingInFirstDirectory)
      
      # Print a message
      print("Out of " + str(len(result.files)) + " files, " + str(mismatchingFilesNumber) + " do NOT match.")
      
      # Log the different files
      for differentFile in result.differentFiles:
        self.logFile.write(str(directoryRoot.joinpath(differentFile)) + ": different\n")
        
      # Log the files only present in the first system
      for fileOnlyInFirstSystem in result.filesMissingInSecondDirectory:
        self.logFile.write(str(directoryRoot.joinpath(fileOnlyInFirstSystem)) + ": only present in the first system\n")
        
      # Log the files only present in the second system
      for fileOnlyInSecondSystem in result.filesMissingInFirstDirectory:
        self.logFile.write(str(directoryRoot.joinpath(fileOnlyInSecondSystem)) + ": only present in the second system\n")
        
      # Flush the log file
      self.logFile.flush()
        
      # Print a message
      print("See " + str(self.logFilePath) + " for the list of mismatching files.")

if __name__ == "__main__":
  # Set the usage examples
  usageExamples =  "Examples:\n"
  usageExamples += "  Compare a Tails key with a downloaded Tails image:\n"
  usageExamples += "    " + sys.argv[0] + " /dev/sdb tails_image.img\n"
  usageExamples += "  Compare two Tails keys:\n"
  usageExamples += "    " + sys.argv[0] + " /dev/sdb /dev/sdc\n"

  # Create the argument parser
  parser = argparse.ArgumentParser(description="Compare two Tails systems.", epilog=usageExamples, formatter_class=argparse.RawDescriptionHelpFormatter)
  parser.add_argument("firstSystem", metavar="first_system", help="first Tails system to compare")
  parser.add_argument("secondSystem", metavar="second_system", help="second Tails system to compare")

  # Parse the arguments
  arguments = parser.parse_args()
  
  # Compare the systems
  try:
    TailsComparison(arguments.firstSystem, arguments.secondSystem)
  # Error during execution of a shell command
  except sh.ErrorReturnCode as errorReturnCode:
   print("Command failed: " + errorReturnCode.full_cmd)
  # Runtime error
  except RuntimeError as runtimeError:
    # Print a message
    print("Runtime error: " + str(runtimeError))

